# whattomine

Csapat tagjai:

Flak Nándor – H45JIU
Hortobágyi Gábor – EOJ0ME
Szabó István – P0TNV7

#Program leírása:
Ethereum bányászási megtérülést számoló alkalmazás. A program indításnál betölti a külső adatbázisból az előre feltöltött grafikus kártyákat, és a hozzájuk tartozó adatokat. Ezen kívül frissíti az éppen aktuális nehézségi fokot, blokkidőt, 1kwh áram árát és az aktuális árfolyamot. Lehetőségünk van saját összeállítást is megadni, amelyet elmenthetünk. Az első lap alján a calculate gombra kattintva kiszámolja a program, hogy adott időintervallum alatt a beadott konfiguráció mekkora termelésre képes, és ezt óránkénti/napi/heti/havi/éves bontásban a felhasználónak megmutatja. A harmadik fülre nyomva lehetőség van különböző oldalakon böngészni az aktuális árfolyamot.
A bal felső sarokban lévő menü gombra kattintva beléphetünk a saját felhasználói fiókunkhoz, amellyel betölthetjük az eltárolt adatainkat (Google azonosítóval használatos), illetve lehetőségünk van értesítések be és kikapcsolására. Értesítések érkezhetnek a nehézségi fok drasztikus emelkedéséről, illetve árfolyamváltozásról.
Továbbiakban mellékeltünk a dokumentumunkhoz néhány előzetes képet a megvalósítandó alkalmazásról, amelyet a fluidui nevű online rendszeren készítettünk el.