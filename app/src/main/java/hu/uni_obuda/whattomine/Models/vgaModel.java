package hu.uni_obuda.whattomine.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Horti on 2017. 11. 23..
 */

public class vgaModel implements Parcelable, Serializable{

    /**
     * Created by Horti on 2017. 11. 23..
     */

    private double hashrate, wattage;
    private String name;

    public vgaModel(double hashrate,String name, double wattage) {
        this.hashrate = hashrate;
        this.wattage = wattage;
        this.name = name;
    }

    protected vgaModel(Parcel in) {
        hashrate = in.readDouble();
        wattage = in.readDouble();
        name = in.readString();
    }

    public static final Creator<vgaModel> CREATOR = new Creator<vgaModel>() {
        @Override
        public vgaModel createFromParcel(Parcel in) {
            return new vgaModel(in);
        }

        @Override
        public vgaModel[] newArray(int size) {
            return new vgaModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public double getHashrate() {
        return hashrate;
    }

    public double getWattage() {
        return wattage;
    }

    @Override
    public String toString() {
        return name.equals("Custom")?name+" H:"+hashrate+" P:"+ wattage :name;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeDouble(hashrate);
        dest.writeDouble(wattage);
        dest.writeString(name);
    }
}
