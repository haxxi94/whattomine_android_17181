package hu.uni_obuda.whattomine.Models;

/**
 * Created by Horti on 2017. 12. 11..
 */

public class CoinChanger {
    private String url;
    private String price;
    private String name;

    public String getImagePath() {
        return imagePath;
    }

    private String imagePath;

    public CoinChanger(String url, String price, String name, String imagePath) {
        this.url = url;
        this.price = price;
        this.name = name;
        this.imagePath=imagePath;

    }

    public String getUrl() {
        return url;
    }

    public String getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }
}
