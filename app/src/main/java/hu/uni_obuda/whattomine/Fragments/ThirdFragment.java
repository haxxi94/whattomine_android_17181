package hu.uni_obuda.whattomine.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import hu.uni_obuda.whattomine.Models.CoinChanger;
import hu.uni_obuda.whattomine.Adapter.CoinAdapter;
import hu.uni_obuda.whattomine.R;

/**
 * Created by flakn on 2017. 11. 14..
 */

public class ThirdFragment extends Fragment {
    private DatabaseReference database;
    private ArrayList<CoinChanger> changers;
    private RecyclerView recyclerView;
    private CoinAdapter coinAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.third_frag, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView= (RecyclerView) view;
        initData();
    }

    private void initData()
    {
        if(this.changers!=null)
        {
            this.changers.clear();
        }
        else
        {
            this.changers= new ArrayList<>();
        }
        database = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference ref = database.child("DB").child("CoinChanger");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren())
                {
                    Log.e("ThirdFragment", "Data: "+data);
                    changers.add(new CoinChanger(data.child("url").getValue().toString(),data.child("price").getValue().toString(),data.getKey(),data.child("imgurl").getValue().toString()));
                }
                updateUI();
                ref.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ref.removeEventListener(this);
            }
        });
    }

    private void updateUI() {
        Log.e("ThirdFragment","ListSize: "+changers.size());
        if (coinAdapter== null && changers.size()>0) {
            coinAdapter = new CoinAdapter(changers);
            coinAdapter.setViewListener(viewListener);
            recyclerView.setAdapter(coinAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }
    }

    private View.OnClickListener viewListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String temp=((AppCompatTextView) (v.findViewById(R.id.ethCoinChangerName))).getText().toString();
            for(CoinChanger cc: changers)
            {
                if(cc.getName().equals(temp))
                {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(cc.getUrl()));
                    startActivity(browserIntent);
                    break;
                }
            }
        }
    };
    public static ThirdFragment newInstance(String text) {

        ThirdFragment f = new ThirdFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }


}
