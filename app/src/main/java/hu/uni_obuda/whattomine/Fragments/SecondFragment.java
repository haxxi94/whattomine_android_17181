package hu.uni_obuda.whattomine.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import hu.uni_obuda.whattomine.Adapter.VgaAdapter;
import hu.uni_obuda.whattomine.Models.vgaModel;
import hu.uni_obuda.whattomine.R;
/**
 * Created by flakn on 2017. 11. 14..
 */

public class SecondFragment extends Fragment {
    private CallbackFromSecond callback;
    private ArrayList<vgaModel> models;
    private AppCompatButton importBut,exportBut;
    private DatabaseReference dataBase;
    private String emailU;
    private boolean result;
    private RecyclerView rv;
    private VgaAdapter vgaAdapter;
    private String emmm="kisgeza@gmail.com";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBase= FirebaseDatabase.getInstance().getReference().child("DB").child("Storage");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.upload_fragment, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.exportBut = (AppCompatButton) view.findViewById(R.id.ExportDB);
        this.importBut = (AppCompatButton) view.findViewById(R.id.ImportDB);
        this.rv= (RecyclerView) view.findViewById(R.id.uploadList);
        init();
    }

    private void exportToFirebase(String id)
    {
        if(dataBase.child(id).getKey()!=null)
        {
            dataBase.child(id).removeValue();
        }
        callSetNewValue(id,0);
        updateUI();
    }
    private void callSetNewValue(String emailId,int childID)
    {
        int temp=childID;
        if(childID<models.size())
        {
            setNewValue(models.get(childID),emailId,temp+1);
        }
    }

    private void setNewValue(vgaModel item, final String emailId, final int childId)
    {
        dataBase.child(emailId).child(String.valueOf(childId)).setValue(item, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                callSetNewValue(emailId,childId);
            }
        });
    }

    private String convertToKey(String email)
    {
        String temp=email.replaceAll("[\\.\\@]","_");
        return temp;
    }
    private void importFromFirebase(String id)
    {
        if(models!=null)
        {
            models.clear();
        }
        else {
            models= new ArrayList<>();
        }
        final DatabaseReference ref = dataBase.child(id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren())
                {
                    double hash = Double.parseDouble(data.child("hashrate").getValue().toString());
                    double wattage=Double.parseDouble(data.child("wattage").getValue().toString());
                    String name = data.child("name").getValue().toString();
                    models.add(new vgaModel(hash,name,wattage));
                }
                callback.getModel(models);
                updateUI();
                ref.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ref.removeEventListener(this);
            }
        });
    }

    private void updateUI()
    {
        if (models!=null && models.size()>0) {
            vgaAdapter = new VgaAdapter(models);
            rv.setAdapter(vgaAdapter);
            rv.setLayoutManager(new LinearLayoutManager(getContext()));
        }
    }

    private void init()
    {

        exportBut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                exportToFirebase(convertToKey(emmm));
            }
        });
        importBut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                importFromFirebase(convertToKey(emmm));

            }
        });
        updateUI();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            Log.e("SecondFragment","Call back to MainActivity");
            callback = (CallbackFromSecond)context;
        }
        catch (ClassCastException e) {Log.e("SecondFrag","MainActivity must implements CallbackFromSecond interface");}
    }

    public static SecondFragment newInstance(String text) {

        SecondFragment f = new SecondFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

    public interface CallbackFromSecond{
        public void getModel(ArrayList<vgaModel> models);
    }
    public void getModels(ArrayList<vgaModel> models)
    {
        this.models=models;
        updateUI();
    }
    public void getEmail(String emailU, boolean result){this.emailU=emailU; this.result=result; updateUI();}

}
