package hu.uni_obuda.whattomine.Fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import hu.uni_obuda.whattomine.Activity.CalculateActivity;
import hu.uni_obuda.whattomine.MainActivity;
import hu.uni_obuda.whattomine.Models.vgaModel;
import hu.uni_obuda.whattomine.R;
/**
 * Created by flakn on 2017. 11. 14..
 */
public class FirstFragment extends Fragment {

    private AppCompatEditText hashRate, power,powerCost,networkHashrate, ethPrice, blockTimeEd,blockRewardEd;
    private AppCompatSpinner spinnerVGAselector,spinnerSelectedVGAs;
    private AppCompatButton addVGA,calculate;

    private DatabaseReference database;

    private ArrayList<String> arrayAvailableVGA=null;
    private ArrayList<vgaModel> arraySelectedVGA=null;

    private double networkHashRate;
    private double blockTime;
    private double blockReward;
    private double eThPrice;
    private CallbackFromFirst raiser;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            raiser= (CallbackFromFirst) context;
        }catch (ClassCastException e){}
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.first_frag,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        hashRate =(AppCompatEditText) view.findViewById(R.id.editHashrate);
        power =(AppCompatEditText) view.findViewById(R.id.editPower);
        powerCost =(AppCompatEditText) view.findViewById(R.id.editedCostKW);

        networkHashrate =(AppCompatEditText) view.findViewById(R.id.editNetworkHashrate);
        networkHashrate.setEnabled(false);
        ethPrice =(AppCompatEditText) view.findViewById(R.id.editedETHPrice);
        ethPrice.setEnabled(false);
        blockRewardEd = (AppCompatEditText) view.findViewById(R.id.editedBlockReward);
        blockRewardEd.setEnabled(false);
        blockTimeEd = (AppCompatEditText) view.findViewById(R.id.editedBlockTime);
        blockTimeEd.setEnabled(false);

        spinnerSelectedVGAs= (AppCompatSpinner) view.findViewById(R.id.spinnerSelectedVGAs);
        spinnerVGAselector = (AppCompatSpinner) view.findViewById(R.id.spinnerVGAselector);

        calculate = (AppCompatButton) view.findViewById(R.id.buttonCalculate);
        addVGA = (AppCompatButton) view.findViewById(R.id.buttonAddSelectedVGA);

        arraySelectedVGA = new ArrayList<>();
        initDataBase();
        init();
    }
    private void initDataBase()
    {
        arrayAvailableVGA = new ArrayList<>();
        arrayAvailableVGA.add("Custom");
        database = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference ref= database.child("DB").child("VideoCards");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("FireBase", "Try to get values from DB, if its not empty");
                for(DataSnapshot postSnapshot :dataSnapshot.getChildren())
                {
                    arrayAvailableVGA.add(String.valueOf(postSnapshot.child("Name").getValue()));
                    Log.e("FireBase",String.valueOf(postSnapshot.child("Name").getValue()));
                }
                ArrayAdapter<String> adapterAvailableVGA = new ArrayAdapter<String>(getContext(),R.layout.support_simple_spinner_dropdown_item,arrayAvailableVGA);
                spinnerVGAselector.setAdapter(adapterAvailableVGA);
                ref.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ref.removeEventListener(this);
            }
        });
        final DatabaseReference actual= database.child("DB").child("Actual");
        actual.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren())
                {
                    if(data.getKey().toString().equals("BlockReward"))
                        blockReward=Double.parseDouble(data.getValue().toString());
                    if(data.getKey().toString().equals("BlockTime"))
                        blockTime=Double.parseDouble(data.getValue().toString());
                    if(data.getKey().toString().equals("NetworkHashrate"))
                        networkHashRate=Double.parseDouble(data.getValue().toString());
                    if(data.getKey().toString().equals("Price"))
                        eThPrice=Double.parseDouble(data.getValue().toString());
                    Log.e("FireBase",data.toString());
                }
                networkHashrate.setText(String.valueOf(networkHashRate));
                ethPrice.setText(String.valueOf(eThPrice));
                blockTimeEd.setText(String.valueOf(blockTime));
                blockRewardEd.setText(String.valueOf(blockReward));
                actual.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                actual.removeEventListener(this);
            }
        });
    }

    public static FirstFragment newInstance(String text) {

        FirstFragment f = new FirstFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);
        f.setArguments(b);

        return f;
    }

    private boolean tryParseDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void init()
    {
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(arraySelectedVGA.size()>0 && tryParseDouble(powerCost.getText().toString()))
                {
                    Log.e("FirstD","Open new CalculateActivity");
                    Intent intent= new Intent(getActivity(), CalculateActivity.class);
                    intent.putExtra("vgaModels",arraySelectedVGA);
                    intent.putExtra("powerCost",Double.parseDouble(powerCost.getText().toString()));
                    intent.putExtra("ethPrice",eThPrice);
                    intent.putExtra("networkHashrate",networkHashRate);
                    intent.putExtra("blockTime",blockTime);
                    intent.putExtra("blockReward",blockReward);
                    Log.e("FirstD","Put extras for CalculateActivity");
                    startActivity(intent);
                    Log.e("FirstD","Start CalculateActivity");
                    updateFragment();
                }
                else if(!tryParseDouble(powerCost.getText().toString()))
                {
                    // TODO: 2017. 12. 08. Megváltoztatni ezt! szép és jó de nem illik ide
                    powerCost.setBackgroundColor(Color.RED);
                }
                else if(arraySelectedVGA.size()==0)
                {
                    Toast toast = Toast.makeText(getContext(), "Select some VGA first!", Toast.LENGTH_SHORT);
                    toast.show();
                    updateFragment();
                }
            }
        });

        addVGA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(spinnerVGAselector.getSelectedItem().toString().equals("Custom") && !tryParseDouble(hashRate.getText().toString()))
                {
                    hashRate.setBackgroundColor(Color.RED);
                }
                if(spinnerVGAselector.getSelectedItem().toString().equals("Custom") && !tryParseDouble(power.getText().toString()))
                {
                    power.setBackgroundColor(Color.RED);
                }
                if(tryParseDouble(hashRate.getText().toString()) && tryParseDouble(power.getText().toString()))
                {
                    if(spinnerVGAselector.getSelectedItem().toString().equals("Custom"))
                    {
                        arraySelectedVGA.add(new vgaModel(Double.parseDouble(hashRate.getText().toString()),"Custom",Double.parseDouble(power.getText().toString())));
                        updateFragment();
                    }
                    else
                    {
                        arraySelectedVGA.add(new vgaModel(Double.parseDouble(hashRate.getText().toString()),spinnerVGAselector.getSelectedItem().toString(),Double.parseDouble(power.getText().toString())));
                    }
                    raiser.getModels(arraySelectedVGA);
                }
                ArrayAdapter<vgaModel> adapterSelectedVGA = new ArrayAdapter<vgaModel>(getContext(),R.layout.support_simple_spinner_dropdown_item,arraySelectedVGA);
                spinnerSelectedVGAs.setAdapter(adapterSelectedVGA);
            }
        });
        //If a new Models.VGA selected, we must update the UI
        spinnerVGAselector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateFragment();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSelectedVGAs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("FirstD", "Item selected from bottom spinner");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    //Have to call every time, when we select a new VGA/Custom
    private void updateFragment()
    {
        if(spinnerVGAselector.getSelectedItem().equals("Custom"))
        {
            hashRate.setEnabled(true);
            hashRate.setText("");
            power.setEnabled(true);
            power.setText("");
        }
        else
        {
            hashRate.setEnabled(false);
            power.setEnabled(false);

            DatabaseReference ref= database.child("DB").child("VideoCards");
            final Query nameQuery = ref.orderByChild("Name").equalTo(String.valueOf(this.spinnerVGAselector.getSelectedItem()));
            nameQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for(DataSnapshot singleSnapshot : dataSnapshot.getChildren())
                    {
                        hashRate.setText(singleSnapshot.child("Hashrate").getValue().toString());
                        power.setText(singleSnapshot.child("Wattage").getValue().toString());
                        Log.e("FireBase", "Try to get selected VGA datas");
                    }
                    nameQuery.removeEventListener(this);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    nameQuery.removeEventListener(this);
                }
            });
        }
        hashRate.setBackgroundColor(Color.TRANSPARENT);
        powerCost.setBackgroundColor(Color.TRANSPARENT);
        power.setBackgroundColor(Color.TRANSPARENT);
        Log.e("FirstD","Fragment UI updated!");
    }
    public interface CallbackFromFirst{
        public void getModels(ArrayList<vgaModel> models);
    }
    public void getModel(ArrayList<vgaModel> models)
    {
        this.arraySelectedVGA=models;
        ArrayAdapter<vgaModel> adapterSelectedVGA = new ArrayAdapter<vgaModel>(getContext(),R.layout.support_simple_spinner_dropdown_item,arraySelectedVGA);
        spinnerSelectedVGAs.setAdapter(adapterSelectedVGA);
        Log.e("ImportFromFireBase","GetModels");
    }
}