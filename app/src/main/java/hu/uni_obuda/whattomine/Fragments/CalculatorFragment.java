package hu.uni_obuda.whattomine.Fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import hu.uni_obuda.whattomine.Models.vgaModel;
import hu.uni_obuda.whattomine.R;
/**
 * Created by flakn on 2017. 11. 14..
 */

public class CalculatorFragment extends Fragment {

    private ArrayList<vgaModel> vgaModels=null;
    private double powerCost;
    private double networkHashRate;
    private double blockTime;
    private double blockReward;
    private double ethPriceUSD;
    private AppCompatTextView incomePerHour,incomePerDay,incomePerWeek,incomePerMonth,incomePerYear;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null)
        {
            vgaModels = getArguments().getParcelableArrayList("vgaModels");
            blockReward =getArguments().getDouble("blockReward");
            blockTime = getArguments().getDouble("blockTime");
            powerCost = getArguments().getDouble("powerCost");
            ethPriceUSD = getArguments().getDouble("ethPrice");
            networkHashRate= getArguments().getDouble("networkHashRate");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.second_frag, container, false);

        /*TextView tv = (TextView) v.findViewById(R.id.tvFragSecond);
        tv.setText(getArguments().getString("msg"));*/

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.incomePerHour = (AppCompatTextView) view.findViewById(R.id.IncomePerHour);
        this.incomePerDay = (AppCompatTextView) view.findViewById(R.id.IncomePerDay);
        this.incomePerWeek = (AppCompatTextView) view.findViewById(R.id.IncomePerWeek);
        this.incomePerMonth = (AppCompatTextView) view.findViewById(R.id.IncomePerMonth);
        this.incomePerYear = (AppCompatTextView) view.findViewById(R.id.IncomePerYear);

        initView();
    }

    private double Calculator(double perWhat)
    {
        double sumHashRate=0,sumPowerConsume=0;
        for(vgaModel item : vgaModels)
        {
            sumHashRate+=item.getHashrate();
            sumPowerConsume+=item.getWattage();
        }

        double howMuchTimeToMineOne=networkHashRate/sumHashRate;
        double howMuchTimeToMineOneCorr= howMuchTimeToMineOne*blockTime;
        double howMuchCanBeMined= perWhat/howMuchTimeToMineOneCorr;
        double profitOverTime=ethPriceUSD*howMuchCanBeMined*blockReward;
        double powerPriceOverTime=(perWhat/3600)*powerCost*(sumPowerConsume/1000);
        return profitOverTime-powerPriceOverTime;
    }

    private void initView()
    {

        Log.e("SecondFragment",vgaModels==null?"VGA: Null!":"VGA: got it!");
        Log.e("SecondFragment","BlockTime: "+blockTime);
        Log.e("SecondFragment","BlockReward: "+blockReward);
        Log.e("SecondFragment","EthPrice: "+ethPriceUSD);
        Log.e("SecondFragment","Power Cost: "+powerCost);
        this.incomePerHour.setText(String.valueOf(Calculator(3600)).substring(0,10)+"$");
        this.incomePerDay.setText(String.valueOf(Calculator(86400)).substring(0,10)+"$");
        this.incomePerWeek.setText(String.valueOf(Calculator(604800)).substring(0,10)+"$");
        this.incomePerMonth.setText(String.valueOf(Calculator(2678400)).substring(0,10)+"$");
        this.incomePerYear.setText(String.valueOf(Calculator(32140800)).substring(0,10)+"$");

    }

    public static CalculatorFragment getInstance(ArrayList<Parcelable> vgaModels, double powerCost, double networkHashRate, double blockTime, double blockReward, double ethPrice)
    {
        CalculatorFragment fragment = new CalculatorFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("vgaModels",vgaModels);
        bundle.putDouble("powerCost",powerCost);
        bundle.putDouble("networkHashRate",networkHashRate);
        bundle.putDouble("ethPrice",ethPrice);
        bundle.putDouble("blockTime",blockTime);
        bundle.putDouble("blockReward",blockReward);
        fragment.setArguments(bundle);
        Log.e("SecondFragment","SecondFragment getInstance");
        return fragment;
    }

}
