package hu.uni_obuda.whattomine.Adapter;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import hu.uni_obuda.whattomine.Models.CoinChanger;
import hu.uni_obuda.whattomine.R;

/**
 * Created by Horti on 2017. 12. 11..
 */

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.CoinViewHolder> {
    private ArrayList<CoinChanger> changers;
    private View.OnClickListener ViewListener;

    public void setViewListener(View.OnClickListener viewListener) {
        ViewListener = viewListener;
    }

    public CoinAdapter(ArrayList<CoinChanger> changers) {
        this.changers = changers;
    }

    @Override
    public CoinViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new CoinViewHolder(View.inflate(viewGroup.getContext(), R.layout.coin_changer_frag, null));
    }

    @Override
    public void onBindViewHolder(CoinViewHolder coinViewHolder, int i) {
        CoinChanger cc = changers.get(i);
        coinViewHolder.name.setText(cc.getName());
        coinViewHolder.price.setText(cc.getPrice()+" USD");
        //Log.e("UriParse",cc.getImagePath());
        //coinViewHolder.ethImg.setImageURI(Uri.parse(cc.getImagePath()));
    }

    @Override
    public int getItemCount() {
        return changers.size();
    }

    class CoinViewHolder extends RecyclerView.ViewHolder
    {
        private AppCompatTextView name, price;
        private AppCompatImageView ethImg;
        private RelativeLayout rl;

        public CoinViewHolder(View itemView) {
            super(itemView);
            this.price= (AppCompatTextView) itemView.findViewById(R.id.CoinPrice);
            this.ethImg = (AppCompatImageView) itemView.findViewById(R.id.eth1);
            this.name = (AppCompatTextView) itemView.findViewById(R.id.ethCoinChangerName);
            this.rl = (RelativeLayout) itemView.findViewById(R.id.CoinChangerView);

            if(ViewListener != null)
            {
                rl.setOnClickListener(ViewListener);
            }
        }
    }
}
