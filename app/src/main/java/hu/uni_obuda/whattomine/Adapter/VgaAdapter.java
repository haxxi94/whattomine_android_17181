package hu.uni_obuda.whattomine.Adapter;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import hu.uni_obuda.whattomine.Models.CoinChanger;
import hu.uni_obuda.whattomine.Models.vgaModel;
import hu.uni_obuda.whattomine.R;

/**
 * Created by Horti on 2017. 12. 13..
 */

public class VgaAdapter extends RecyclerView.Adapter<VgaAdapter.VgaViewHolder>{
    private ArrayList<vgaModel> models;


    public VgaAdapter(ArrayList<vgaModel> models) {
        this.models = models;
        Log.e("VgaAdapter","Package size: "+models.size());
    }

    @Override
    public VgaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VgaAdapter.VgaViewHolder(View.inflate(parent.getContext(), R.layout.vga_view, null));
    }

    @Override
    public void onBindViewHolder(VgaViewHolder holder, int position) {
        vgaModel vm= models.get(position);
        holder.name.setText(vm.getName());
        holder.hash.setText(String.valueOf(vm.getHashrate()));
        holder.wattage.setText(String.valueOf(vm.getWattage()));
        if(vm.getName().substring(0,3).equals("AMD"))
        {
            holder.cardImg.setImageResource(R.drawable.amd);
        }
        else if(vm.getName().substring(0,3).equals("GTX"))
        {
            holder.cardImg.setImageResource(R.drawable.nvidia);
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }


    class VgaViewHolder extends RecyclerView.ViewHolder
    {
        private AppCompatTextView name, hash,wattage;
        private AppCompatImageView cardImg;

        public VgaViewHolder(View itemView) {
            super(itemView);
            this.name= (AppCompatTextView) itemView.findViewById(R.id.vgaName);
            this.hash = (AppCompatTextView) itemView.findViewById(R.id.vgaHashrate);
            this.wattage = (AppCompatTextView) itemView.findViewById(R.id.vgaWattage);
            this.cardImg= (AppCompatImageView) itemView.findViewById(R.id.vgaImage);
        }
    }
}
