package hu.uni_obuda.whattomine;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import hu.uni_obuda.whattomine.Activity.LoginActivity;
import hu.uni_obuda.whattomine.Fragments.CalculatorFragment;
import hu.uni_obuda.whattomine.Fragments.FirstFragment;
import hu.uni_obuda.whattomine.Fragments.SecondFragment;
import hu.uni_obuda.whattomine.Fragments.ThirdFragment;
import hu.uni_obuda.whattomine.Models.vgaModel;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,FirstFragment.CallbackFromFirst, SecondFragment.CallbackFromSecond,
                    MyEmail.OnLoggingResultChanged{
    //branch created
    //added comment to create branch
    private FirstFragment firstFrag;
    private SecondFragment secondFragment;
    private AppCompatTextView userUI;
    private String emailUser;
    private boolean loginResult;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //please update tge google-services.json file need Google Sign-In too.

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        userUI= (AppCompatTextView) findViewById(R.id.textUserEmailView);

        ViewPager pager = (ViewPager) findViewById(R.id.viewPager);
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navLogin) {
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void getModels(ArrayList<vgaModel> models) {
        this.secondFragment.getModels(models);
    }

    @Override
    public void getModel(ArrayList<vgaModel> models) {
        this.firstFrag.getModel(models);
    }

    @Override
    public void getResultDatas(String emailU, boolean result) {
        this.emailUser=emailU;
        this.loginResult=result;
        this.userUI.setText(emailUser);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:
                    firstFrag =FirstFragment.newInstance("FirstFragment, Instance 1");
                    return firstFrag;
                case 1:
                    secondFragment =SecondFragment.newInstance("SecondFragment, Instance 1");
                    return secondFragment;
                case 2:
                    return ThirdFragment.newInstance("ThirdFragment, Instance 1");
                default:
                    return ThirdFragment.newInstance("ThirdFragment, Default");
            }
        }

        /**
         * Magic number : 3.
         * @return
         */
        @Override
        public int getCount() {
            return 3;
        }

        /**
         * Tabs in the app.
         * @param position 0 : CALC 1 : INCOME 2 : EXCHANGE
         * @return The actual tab
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {

                case 0:
                    return "CALCULATOR";
                case 1:
                    return "CONFIGURATION";
                case 2:
                    return "EXCHANGE";
                default:
                    return "TITLE";
            }
        }
    }
}
