package hu.uni_obuda.whattomine.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import hu.uni_obuda.whattomine.Fragments.CalculatorFragment;
import hu.uni_obuda.whattomine.Fragments.SecondFragment;
import hu.uni_obuda.whattomine.R;

/**
 * Created by Horti on 2017. 12. 06..
 */

public class CalculateActivity extends AppCompatActivity {
    private FragmentManager fragmentManager;
    private Bundle getExtras;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Horti","Calculator activity on created!");
        setContentView(R.layout.activity_calculate);
        getExtras = getIntent().getExtras();
        initCalculator();
    }

    private void initCalculator() {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        CalculatorFragment secondFragment = CalculatorFragment.getInstance(getExtras.getParcelableArrayList("vgaModels"),getExtras.getDouble("powerCost"),
                getExtras.getDouble("networkHashrate"), getExtras.getDouble("blockTime"),getExtras.getDouble("blockReward"),getExtras.getDouble("ethPrice"));
        fragmentTransaction.add(R.id.calculated, secondFragment);
        fragmentTransaction.commit();
    }
}
