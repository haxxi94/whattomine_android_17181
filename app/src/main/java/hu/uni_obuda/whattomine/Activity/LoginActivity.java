package hu.uni_obuda.whattomine.Activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import hu.uni_obuda.whattomine.Fragments.LoginFragment;
import hu.uni_obuda.whattomine.MainActivity;
import hu.uni_obuda.whattomine.MyEmail;
import hu.uni_obuda.whattomine.R;

public class LoginActivity extends AppCompatActivity implements LoginFragment.CallbackFromLoginFragment{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new LoginFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public void getLoginResult(boolean result, String emailU) {
        ((MyEmail) this.getApplication()).setEmailU(emailU);
        ((MyEmail) this.getApplication()).setResult(result);

    }
}