package hu.uni_obuda.whattomine;

import android.app.Application;
import android.app.usage.UsageEvents;

/**
 * Created by Horti on 2017. 12. 13..
 */

public class MyEmail extends Application {
    public void setEmailU(String emailU) {
        this.emailU = emailU;
    }

    public String getEmailU() {

        return emailU;
    }

    String emailU;
    boolean result=false;
    private OnLoggingResultChanged listener;

    public boolean isResult() {
        if(listener!=null)
        {
            listener.getResultDatas(emailU,result);
        }
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
    public interface OnLoggingResultChanged
    {
        public void getResultDatas(String emailU,boolean result);
    }

    public void AddOnLoggingResultListener(OnLoggingResultChanged listener)
    {
        this.listener=listener;
    }

}
